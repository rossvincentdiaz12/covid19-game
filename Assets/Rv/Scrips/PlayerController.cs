﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float moveSpeed;
    private float moveVelocity;
    public float jumpHeight;

    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    private bool grounded;

    private Animator anim;

    public float knockBack;
    public float knockBackLength;
    public float knockBackCount;
    public bool knockFronRigth;


    

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
       
     

    }

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
    }

    // Update is called once per frame
    void Update () {

        if (grounded)
            anim.SetBool("Grounded",grounded);

        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
        }

        //player movement
       

        //left player movement
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //speed = -speedX;
            Moveleft();
        }
        
        //

        //rigth player movement
        if (Input.GetKey(KeyCode.RightArrow))
        {
            //speed = speedX;
            Moverigth();
        }
       
      
        anim.SetFloat("Speed", Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x)); 
        //animation left and rigth
        if (GetComponent<Rigidbody2D>().velocity.x > 0)
           transform.localScale = new Vector3(1f, 1f, 1f);
        else if (GetComponent<Rigidbody2D>().velocity.x < 0)
           transform.localScale = new Vector3(-1f, 1f, 1f);
    }

  

    public void jump()
    {
        if (grounded) {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpHeight);
        }
    }

    public void Moverigth()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
    }

    public void Moveleft()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
    }

    public void nomove()
    {
        moveSpeed = 0;
    }
}
