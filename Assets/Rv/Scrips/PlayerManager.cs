﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour {


    public LevelManager levelManager;

    public float speedX;
    public float jumpSpeedY;

    bool facingRight, Jumping;
    float speed;


    public Transform groundCheck;
    public float groundCheckRadius;
    public LayerMask whatIsGround;
    private bool grounded;

    Animator anim;//animation
    Rigidbody2D rb;

    public int count;//moon count
    public Text MoonCount;
    public Text winText;
    public GameObject infoCanvas;

    public Text enemycount;

    public Text bossingcount;

    public Transform firePoint;
    public GameObject ninjaStar;

    public AudioSource jumpSound;
    public AudioSource death;
    public GameObject pickupeffect;

    public GameObject boss;

    // Use this for initialization
    void Start ()
    {
        anim = GetComponent<Animator>();//animation
        rb = GetComponent<Rigidbody2D>();
        facingRight = true;
        levelManager = FindObjectOfType<LevelManager>();
        moonCount();
        SetCountText();
        bosscount();
    }

    void FixedUpdate()
    {
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
    }

    // Update is called once per frame
    void Update ()
    {
        //player movement
        MovePlayer(speed);

        //left player movement
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            speed = -speedX;
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            speed = 0;
        }
        //

        //rigth player movement
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            speed = speedX;
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            speed = 0;
        }
        //

        if (Input.GetKeyDown(KeyCode.UpArrow)&& grounded)
        {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpSpeedY);
            jumpSound.Play();
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Instantiate(ninjaStar, firePoint.position, firePoint.rotation);
        }
        //to put some friction in the wall and ground if the player is not jumping
        //GetComponent<Rigidbody2D>().velocity = new Vector2(jumpSpeedY, GetComponent<Rigidbody2D>().velocity.y);

        anim.SetFloat("Speed", Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x));//animation
        //animation
        if (GetComponent<Rigidbody2D>().velocity.x > 0)
            transform.localScale = new Vector3(2.069378f, 2.673627f, 1);
        else if (GetComponent<Rigidbody2D>().velocity.x < 0)
            transform.localScale = new Vector3(-2.069378f, 2.673627f, 1);

    }

    void OnCollisionEnter2D(Collision2D col)
    {
     if(col.gameObject.tag.Equals("tutorial"))
        {
            Application.LoadLevel("Menu");
        }
        if (col.gameObject.tag.Equals("level1border"))
        {
            Application.LoadLevel("finalLevel1");
        }
        if (col.gameObject.tag.Equals("presistentmactan"))
        {
            Application.LoadLevel("finalLevel2");
        }
        if (col.gameObject.tag.Equals("bossbattle"))
        {
            Application.LoadLevel("finalBossBattle");
        }
        if (col.gameObject.tag.Equals("spike"))
        {
            //Debug.Log("dead");
           levelManager.respawnPlayer();
            death.Play();
          
        }
        if (col.gameObject.tag.Equals("enemy"))
        {
            //Debug.Log("dead");
            levelManager.respawnPlayer();
            death.Play();

        }

        if (col.gameObject.tag.Equals("pickup"))
        {
            Instantiate(pickupeffect, col.transform.position, col.transform.rotation);
            col.gameObject.SetActive(false);
        }

        if (col.gameObject.tag.Equals("moon"))
        {
            Instantiate(pickupeffect, col.transform.position, col.transform.rotation);
            Destroy(col.gameObject);
            count = count + 1;
            moonCount();
        }
    }

    void MovePlayer(float playerSpeed)
    {
        //code for player movement
        rb.velocity = new Vector3(speed ,rb.velocity.y,0);
    }
    //methods for player touch movement 
    public void walkLeft()
    {
        speed = -speedX;
    }


    public void walkRight()
    {
        speed = speedX;
    }

    public void stopMoving()
    {
        speed = 0;
    }

    public void jump()
    {
        if (grounded) {
            GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpSpeedY);
            jumpSound.Play();
        }
    }

    public void fire()
    {
        Instantiate(ninjaStar, firePoint.position, firePoint.rotation);
    }

    void moonCount()
    {
        MoonCount.text = "Moons Collected :" + count.ToString() + "/7";
        if (count >= 7)
        {
            Debug.Log("ok");
            winText.text = "Portal is Open !";
            infoCanvas.SetActive(true);
        }
    }
    public void SetCountText()
    {

        enemycount.text = "Virus Killed: " + count.ToString() + "/5";

        if (count >= 5)
        {
            Debug.Log("win");
            winText.text = "You can proceed to next level";
            infoCanvas.SetActive(true);
        }

    }
    public void bosscount()
    {

        bossingcount.text = "Boss: " + count.ToString() + "/50";

        if (count >= 50)
        {
            Destroy(boss);
            Debug.Log("win");
            winText.text = "You can proceed to next level";
            infoCanvas.SetActive(true);
        }

    }
}
