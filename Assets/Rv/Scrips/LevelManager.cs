﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {


    public GameObject currentCheckpoint;
    public GameObject char1;
    private PlayerManager player;
    public GameObject gameoverCanvas;

    public float respawnDelay;
	// Use this for initialization
	void Start () {
        player = FindObjectOfType<PlayerManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void respawnPlayer()
    {
        player.gameObject.SetActive(false);
        gameoverCanvas.SetActive(true);
        Debug.Log("Player Respawn");
        
        
    }

   
   
}
