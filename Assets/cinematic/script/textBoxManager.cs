﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textBoxManager : MonoBehaviour {

    public GameObject textBox;
    public GameObject manager,left,rigth,jump,attack,pause;
    public Text atext;

    public int currentLine;
    public int endLine;

    public PlayerController player;

    public TextAsset textFile;

    public string[] textLines;
    // Use this for initialization
    void Start()
    {
         Time.timeScale = 0f;
        if (textFile != null)
        {
            textLines = (textFile.text.Split('\n'));
        }
        if (endLine == 0)
        {
            endLine = textLines.Length - 1;
        }
       
           

        
    }
    void Update()
    {
        atext.text = textLines[currentLine];

       

    }

    public void Plus()
    {

        
            currentLine += 1;
        if (currentLine > endLine)
        {
            textBox.SetActive(false);
            atext.text = "";
            Time.timeScale = 1f;
            left.SetActive(true);
            rigth.SetActive(true);
            jump.SetActive(true);
            attack.SetActive(true);
            pause.SetActive(true);
            //Application.LoadLevel("finalLevel1scene");
           
            Destroy(manager);
        }

    }
}
