﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bossScript : MonoBehaviour {

    public float moveSpeed;
    public bool moveRigth;

    public Transform wallCheck;
    public float wallCheckRadius;
    public LayerMask whatIsWall;
    private bool hittingWall;

    public int count;//moon count
    public Text MoonCount;
    public Text winText;
    public GameObject infoCanvas;

    //private bool atEdge;
    //public Transform edgeCheck;

    // Use this for initialization
    void Start()
    {

    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag.Equals("weapon"))
        {
           // Instantiate(pickupeffect, col.transform.position, col.transform.rotation);
           // Destroy(col.gameObject);
            count = count + 1;
            moonCount();
        }
    }
    // Update is called once per frame
    void Update()
    {

        // moving the AI rigth if theres a wall and left and right if it reach the edge
        hittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
        //atEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);
        if (hittingWall)

            moveRigth = !moveRigth;


        if (moveRigth)
        {
           // GetComponent<Rigidbody2D>() = new Vector3 (GetComponent<Rigidbody2D>().velocity.y.moveSpeed)
            //animation
            transform.localScale = new Vector3(-4.24252f, 3.887955f, 1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            //animation
            transform.localScale = new Vector3(4.24252f, 3.887955f, 1f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    void moonCount()
    {
        MoonCount.text = "Moons Collected :" + count.ToString() + "/7";
        if (count >= 7)
        {
            Debug.Log("ok");
            winText.text = "Portal is Open !";
            infoCanvas.SetActive(true);
        }
    }
}
