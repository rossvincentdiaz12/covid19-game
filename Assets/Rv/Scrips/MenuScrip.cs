﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScrip : MonoBehaviour {
    public GameObject pauseMenuCanvas;
    // Use this for initialization
    void Start () {

        //Time.timeScale = 0f;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void menu()
    {
        Application.LoadLevel("FinalMainMenu");
        //Time.timeScale = 1f;
    }

    public void StartGame()
    {

        Application.LoadLevel("finalLevel1");

    }

    public void Quit()
    {
        Application.Quit();
        Debug.Log("Exit");
    }

    public void statlevel()
    {
        Application.LoadLevel("levelselect");
    }

    public void mactanmainlevel1()
    {
        Application.LoadLevel("finalLevel1");
    }

    public void ispause()
    { 
        pauseMenuCanvas.SetActive(true);
        Time.timeScale = 0f;
    }

    public void level2()
    {
        Application.LoadLevel("finalLevel2");
    }

    public void level3()
    {
        Application.LoadLevel("finalLevel3");
    }

     public void level4()
    {
        Application.LoadLevel("finalLevel4");
    }

    public void level5()
    {
        Application.LoadLevel("finalBossBattle");
    }

    public void lvl1scene()
    {
        Application.LoadLevel("scene");
    }
    //public void isnotpause()
    //{
    //    pauseMenuCanvas.SetActive(false);
    //    Time.timeScale = 1f;
    //}
}
