﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class enemicount : MonoBehaviour {

    public static int enemycount;

    Text enemiCount;

     void Start()
    {
        enemiCount = GetComponent<Text>();
        enemycount = 0;
    }

    void Update()
    {
        if (enemycount < 0)
            enemycount = 0;

        enemiCount.text = " " + enemycount;
    }
    public static void AddPoints (int pointsToAdd)
    {
        enemycount += pointsToAdd;
    }
}
