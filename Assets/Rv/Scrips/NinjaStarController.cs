﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaStarController : MonoBehaviour {


    public float speed;//to move the arrow
    public PlayerManager player;//to find the 
    public GameObject enemyDeathEffect;//to instantsiate the death particle
    public GameObject impactEffect;// to instantsiate the weapon destroy
   
	// Use this for initialization
	void Start () {
        //to find the object where the player manager script were
		player=FindObjectOfType<PlayerManager>();
        //to determine if the player is on the rigth so the weapon will move where the player is facing
        if (player.transform.localScale.x < 0)
            speed = -speed;
	}
	
	// Update is called once per frame
	void Update () 
    {
        //
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
        //animation of the weapon to the rigth
        if (GetComponent<Rigidbody2D>().velocity.x > 0)
            transform.localScale = new Vector3(1, 1, 1);
        //animation of the weapon to the left
        else if (GetComponent<Rigidbody2D>().velocity.x < 0)
            transform.localScale = new Vector3(-1, 1, 1);
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        // to find if the enemy collide with the player
        if (col.gameObject.tag.Equals("enemy"))
        {
           //to instantiate the enemy destroy
            Instantiate(enemyDeathEffect, col.transform.position, col.transform.rotation);
            //to count player kill
            player.count = player.count + 1;
            player.SetCountText();
            Destroy(col.gameObject);
        }
        // to find if the boss collide with the player
        if (col.gameObject.tag.Equals("boss"))
        {
            //to count the life
            player.count = player.count + 1;
            player.bosscount();
        }
            Destroy(gameObject);
        Instantiate(impactEffect, col.transform.position, col.transform.rotation);   

    }
}
