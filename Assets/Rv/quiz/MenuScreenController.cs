﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuScreenController : MonoBehaviour
{


    public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void Menu()
    {
        SceneManager.LoadScene("Persistent");
    }

    public void StartPlatform()
    {
        SceneManager.LoadScene("Level 1");
    }
    public void level2()
    {
        SceneManager.LoadScene("lvl2r1");
    }

    public void level3()
    {
        SceneManager.LoadScene("lvl3r1");
    }

    public void level4()
    {
        SceneManager.LoadScene("lvl4r1");
    }

    public void level5()
    {
        SceneManager.LoadScene("lvl5r1");
    }

    public void QuitGame()
    {
        Debug.Log("Game Exit");
        Application.Quit();
    }

    public void ViewDataScore()
    {
        Debug.Log("Database");
        SceneManager.LoadScene("Scores");
    }

    public void SelectLevel()
    {
        SceneManager.LoadScene("LevelSelect");
    }

    public void ScoreView()
    {
        SceneManager.LoadScene("Scores");
    }

    public void Quiz()
    {
        SceneManager.LoadScene("Quizlevel");
    }
}