﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InfoMenuControllScript : MonoBehaviour {

	public Button info1,info2,info3;
	
	public int levelPassed;

	// Use this for initialization
	void Start () {
		levelPassed = PlayerPrefs.GetInt ("LevelPassed");
		info1.interactable = false;
		info2.interactable = false;
		info3.interactable = false;

		switch (levelPassed) {
		case 1:
			info1.interactable = true;
			break;
		case 2:
			info1.interactable = true;
			info2.interactable = true;
			break;
		case 3:
			info1.interactable = true;
			info2.interactable = true;
			info3.interactable = true;
			break;
		}
	}
	
	// public void levelToLoad (int level)
	// {
	// 	SceneManager.LoadScene (level);
	// }

	public void resetPlayerPrefs()
	{
		info1.interactable = false;
			info2.interactable = false;
			info3.interactable = false;
		PlayerPrefs.DeleteAll ();
	}
}
