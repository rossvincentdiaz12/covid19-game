﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControls : MonoBehaviour {

    private PlayerController theplayer;
    
	// Use this for initialization
	void Start () {
        theplayer = FindObjectOfType<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void leftarrow()
    {
        theplayer.Moveleft();
    }
    public void rigtharrow()
    {
        theplayer.Moverigth();
    }
    public void unpressarrow()
    {
        theplayer.nomove();
    }
    public void jump()
    {
        theplayer.jump();
    }
}
